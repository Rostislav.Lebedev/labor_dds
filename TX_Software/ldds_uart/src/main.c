#include <xparameters.h>
#include <stdint.h>
#include <sleep.h>

// Pointer auf Slave-Register
volatile uint32_t* slv0 = (uint32_t*)(XPAR_MY_UART_0_S00_AXI_BASEADDR); 	// RX
volatile uint32_t* slv1 = (uint32_t*)(XPAR_MY_UART_0_S00_AXI_BASEADDR+4);	// TX
volatile uint32_t* slv2 = (uint32_t*)(XPAR_MY_UART_0_S00_AXI_BASEADDR+8); 	// Stati

// Pointer auf LED-Register
volatile uint32_t* led0_color = (uint32_t*)(XPAR_MY_RGB_LED_0_S00_AXI_BASEADDR);

void txSend(char text[],int size);
void echo();
void UART_LED_control();
uint32_t checkColor(char puffer[]);
int convert_color(char farbe[]);

void main(void)
{
	//echo();

	UART_LED_control();
}

/*
 * Test der TX-Funktionalit�t
 * Die Funktion empf�ngt den zu sendenden Text als Char-Array und die Gr��e dieses Arrays.
 */
void txSend(char text[],int size)
{
	uint32_t tx_fifo_empty = 0;
	int i = 0;

	if(size > 1)
	{
		while(i < size)
		{
			do {
				tx_fifo_empty = *slv2 & 0x4;
			} while (tx_fifo_empty == 0);
			*slv1 = text[i];
			i++;
		}
	}
	else
	{
		do {
			tx_fifo_empty = *slv2 & 0x4;
		} while (tx_fifo_empty == 0);
		*slv1 = text;
	}
}

/*
 * Einfache Echo-Funktion
 * Das �ber TX empfangene Zeichen wird �ber RX zur�ckgegeben
 */
void echo()
{
	char text[] = "UART-Echo-Funktion.\n";
	int size = sizeof(text)/sizeof(text[0]);
	txSend(text,size-1);

	char byte_puffer = '0';
	uint32_t rx_fifo_valid_data = 0;

	while (1) {
		rx_fifo_valid_data = *slv2 & 0x1;
		if (rx_fifo_valid_data != 0) {
			byte_puffer = *slv0;
			txSend(byte_puffer,1);
		}
	}
}

/*
 * Mithilfe dieser Funktion l�sst sich die Farbe der Board-LED steuern.
 * Die Funktion tastet den Slave-Register 2 ab und wartet auf ein Zeichen.
 * Sobald 6 Zeichen empfangen werden, wird der Farbcode an den RGB-LED-Register �bergeben.
 * Handelt es sich um einen fehlerhaften Farcode, wird eine Fehlermeldung gesendet.
 */
void UART_LED_control()
{
	char text[] = "Senden Sie einen Hex-Farbcode zwischen x000000 und xFFFFFF.\n";
	int sizeText = sizeof(text)/(sizeof(text[0]));
	txSend(text,sizeText-1);

	int zaehler = 0;
	char farbe[7];
	char fehlermeldung[] = "\nFehlerhafter Farbcode: ";
	char bestaetigung[] = "\nFarbe gew�hlt: ";
	uint32_t rx_fifo_valid_data = 0;
	uint32_t color_hex = 0x0;

	while(1)
	{
		while(zaehler != 6)
		{
			do
			{
				rx_fifo_valid_data = *slv2 & 0x1;
			}	while(rx_fifo_valid_data == 0);
			farbe[zaehler] = *slv0;
			zaehler += 1;
		}

		color_hex = convert_color(farbe);

		if(color_hex != -1)
		{
			*led0_color = color_hex;
			txSend(bestaetigung, sizeof(bestaetigung)/sizeof(bestaetigung[0])-1);
		}
		else
			txSend(fehlermeldung, sizeof(fehlermeldung)/sizeof(fehlermeldung[0])-1);

		farbe[6] = 0xd; // 0xd = carriage return in ascii
		txSend(farbe, 7);
		zaehler = 0;
	}
}


/*
 * Die Funktion nimmt ein Char-Array entgegen und �berpr�ft, ob es sich um eine legitime Farbe handelt.
 * Die Farben zwischen 0x000000 und 0xFFFFFF werden akzeptiert.
 * Das Char-Array wird in einen entsprechenden Integer-Wert konvertriert.
 * Kleinbuchstaben (a...f) sind erlaubt.
 * Die �bergebene Farbe wird in eine Zahl umgerechnet und zur�ckgegeben.
 * Im Fehlerfall (keine g�ltige Farbe) wird -1 zur�ckgegeben.
 */
int convert_color(char farbe[])
{
	//char test_farbe[6] = "00FF00";
	int farben_conv = 0;
	int temp = 0;

	int idx = 0;

	for(int i = 5; i >= 0; i--)
	{
		switch(farbe[i])
		{
			case '0':
				temp = 0;
				break;
			case '1':
				temp = 1;
				break;
			case '2':
				temp = 2;
				break;
			case '3':
				temp = 3;
				break;
			case '4':
				temp = 4;
				break;
			case '5':
				temp = 5;
				break;
			case '6':
				temp = 6;
				break;
			case '7':
				temp = 7;
				break;
			case '8':
				temp = 8;
				break;
			case '9':
				temp = 9;
				break;
			case 'A':
				temp = 10;
				break;
			case 'B':
				temp = 11;
				break;
			case 'C':
				temp = 12;
				break;
			case 'D':
				temp = 13;
				break;
			case 'E':
				temp = 14;
				break;
			case 'F':
				temp = 15;
				break;
			case 'a':
				temp = 10;
				break;
			case 'b':
				temp = 11;
				break;
			case 'c':
				temp = 12;
				break;
			case 'd':
				temp = 13;
				break;
			case 'e':
				temp = 14;
				break;
			case 'f':
				temp = 15;
				break;
			default:
				temp = -1;
				break;
		}
		if(temp != -1)
		{
			int fak = 1;
			for(int j = 0; j<idx; j++)
			{
				fak = fak*16;
			}
			farben_conv += temp*fak;
			idx++;
		}
		else
			return -1;
	}
	return farben_conv;
}




