library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

entity testbench_axi is
    generic (
        C_DATA_WIDTH : integer := 32;
        C_ADDR_WIDTH : integer := 4
    );
end entity;

architecture Behavioral of testbench_axi is

    -- AXI4-lite bus signals
    -- Global
    signal ACLK    : std_logic := '0';
    signal ARESETn : std_logic := '1';
    -- Write address channel
    signal AWADDR  : std_logic_vector(C_ADDR_WIDTH - 1 downto 0) := (others => '0');
    signal AWPROT  : std_logic_vector(2 downto 0)                := (others => '0');
    signal AWVALID : std_logic                                   := '0';
    signal AWREADY : std_logic;
    -- Write data channel
    signal WDATA  : std_logic_vector(C_DATA_WIDTH - 1 downto 0)     := (others => '0');
    signal WSTRB  : std_logic_vector((C_DATA_WIDTH/8) - 1 downto 0) := (others => '0');
    signal WVALID : std_logic                                       := '0';
    signal WREADY : std_logic;
    -- Write response channel
    signal BREADY : std_logic := '0';
    signal BRESP  : std_logic_vector(1 downto 0);
    signal BVALID : std_logic;
    -- Read address channel
    signal ARADDR  : std_logic_vector(C_ADDR_WIDTH - 1 downto 0) := (others => '0');
    signal ARPROT  : std_logic_vector(2 downto 0)                := (others => '0');
    signal ARVALID : std_logic                                   := '0';
    signal ARREADY : std_logic;
    -- Read data channel
    signal RREADY : std_logic := '0';
    signal RDATA  : std_logic_vector(C_DATA_WIDTH - 1 downto 0);
    signal RRESP  : std_logic_vector(1 downto 0);
    signal RVALID : std_logic;

    -- helper signals
    constant clk_period  : time      := 20 ns;
    signal rx            : std_logic := '1';
    signal tx            : std_logic;
    signal ascii_char    : std_logic_vector(7 downto 0)  := x"41";
    signal register_data : std_logic_vector(31 downto 0) := (others => '0');

    component my_uart_v1_0 is
        generic (
            C_S00_AXI_DATA_WIDTH : integer := 32;
            C_S00_AXI_ADDR_WIDTH : integer := 4
        );
        port (
            rx              : in std_logic;
            tx              : out std_logic;
            s00_axi_aclk    : in std_logic;
            s00_axi_aresetn : in std_logic;
            s00_axi_awaddr  : in std_logic_vector(C_S00_AXI_ADDR_WIDTH - 1 downto 0);
            s00_axi_awprot  : in std_logic_vector(2 downto 0);
            s00_axi_awvalid : in std_logic;
            s00_axi_awready : out std_logic;
            s00_axi_wdata   : in std_logic_vector(C_S00_AXI_DATA_WIDTH - 1 downto 0);
            s00_axi_wstrb   : in std_logic_vector((C_S00_AXI_DATA_WIDTH/8) - 1 downto 0);
            s00_axi_wvalid  : in std_logic;
            s00_axi_wready  : out std_logic;
            s00_axi_bresp   : out std_logic_vector(1 downto 0);
            s00_axi_bvalid  : out std_logic;
            s00_axi_bready  : in std_logic;
            s00_axi_araddr  : in std_logic_vector(C_S00_AXI_ADDR_WIDTH - 1 downto 0);
            s00_axi_arprot  : in std_logic_vector(2 downto 0);
            s00_axi_arvalid : in std_logic;
            s00_axi_arready : out std_logic;
            s00_axi_rdata   : out std_logic_vector(C_S00_AXI_DATA_WIDTH - 1 downto 0);
            s00_axi_rresp   : out std_logic_vector(1 downto 0);
            s00_axi_rvalid  : out std_logic;
            s00_axi_rready  : in std_logic
        );
    end component my_uart_v1_0;

begin

    uut : my_uart_v1_0
    generic map(
        C_S00_AXI_DATA_WIDTH => C_DATA_WIDTH,
        C_S00_AXI_ADDR_WIDTH => C_ADDR_WIDTH
    )
    port map(
        rx              => rx,
        tx              => tx,
        s00_axi_aclk    => ACLK,
        s00_axi_aresetn => ARESETn,
        s00_axi_awaddr  => AWADDR,
        s00_axi_awprot  => AWPROT,
        s00_axi_awvalid => AWVALID,
        s00_axi_awready => AWREADY,
        s00_axi_wdata   => WDATA,
        s00_axi_wstrb   => WSTRB,
        s00_axi_wvalid  => WVALID,
        s00_axi_wready  => WREADY,
        s00_axi_bresp   => BRESP,
        s00_axi_bvalid  => BVALID,
        s00_axi_bready  => BREADY,
        s00_axi_araddr  => ARADDR,
        s00_axi_arprot  => ARPROT,
        s00_axi_arvalid => ARVALID,
        s00_axi_arready => ARREADY,
        s00_axi_rdata   => RDATA,
        s00_axi_rresp   => RRESP,
        s00_axi_rvalid  => RVALID,
        s00_axi_rready  => RREADY
    );

    -- clock and reset generation
    ACLK    <= not ACLK after (clk_period / 2);
    ARESETn <= '0', '1' after clk_period * 5;

    -- simulates the axi master (processor running software) interfacing with the slave
    simulate_axi_master : process

        -- simulates writing to an axi slave register
        procedure write_register (
            constant register_address : in std_logic_vector(1 downto 0);
            constant data_in          : in std_logic_vector(31 downto 0)
        ) is
        begin
            wait until rising_edge(ACLK);
            -- setup write address
            AWADDR(3 downto 2) <= register_address;
            AWVALID            <= '1';
            -- setup write data
            WDATA  <= data_in;
            WVALID <= '1';
            WSTRB  <= b"1111";

            -- wait for slave
            wait until AWREADY = '1' and WREADY = '1';
            wait until rising_edge(ACLK);
            -- indicate that the master can accept a write response
            BREADY <= '1';

            -- wait for slave response and reset signals
            wait until BVALID = '1';
            wait until rising_edge(ACLK);
            AWVALID <= '0';
            WVALID  <= '0';
            wait until BVALID = '0';
            wait until rising_edge(ACLK);
            BREADY <= '0';
            WSTRB  <= b"0000";
            
            wait until rising_edge(ACLK);
        end procedure;

        -- simulates reading from an axi slave register
        procedure read_register (
            constant register_address : in std_logic_vector(1 downto 0);
            signal data_out           : out std_logic_vector(31 downto 0)
        ) is
        begin
            wait until rising_edge(ACLK);
            -- setup read address
            ARADDR(3 downto 2) <= register_address;
            ARVALID            <= '1';

            -- wait for slave response
            wait until RVALID = '1';
            wait until rising_edge(ACLK);
            data_out <= RDATA;
            ARVALID <= '0';
            
            wait until rising_edge(ACLK);
        end procedure;

    begin
        AWVALID       <= '0';
        WVALID        <= '0';
        BREADY        <= '0';
        ARVALID       <= '0';
        RREADY        <= '1';
        ascii_char    <= x"41";
        register_data <= (others => '0');

        wait until ARESETn = '1';

        while ascii_char < x"5B" loop

            read_register(b"10", register_data);
            if register_data(2) = '1' then
                write_register(b"01", x"000000" & ascii_char);
                ascii_char <= std_logic_vector(unsigned(ascii_char) + 1);
            end if;
            
            wait for clk_period * 10;

        end loop;
        wait;
    end process;

end Behavioral;