library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity testbench_rx is
end entity;

--architecture Behavioral of testbench_rx is
architecture Verhalten of testbench_rx is
    
    constant BAUD_9600: time := 105 us;
    constant BAUD_9600_f: time := 120 us;
    constant clk_period  : time      := 20 ns;
    
    signal clk                : std_logic                    := '0';
    signal reset_n            : std_logic                    := '0';
    signal rx                 : std_logic                    := '1';
    signal rx_fifo            : std_logic_vector(7 downto 0) := (others => '0');
    signal rx_fifo_valid_data : std_logic                    := '0';
    signal rx_fifo_read       : std_logic                    := '0';

    component uart_rx is
        port (
            clk                : in std_logic;
            reset_n            : in std_logic;
            rx                 : in std_logic;
            rx_fifo            : out std_logic_vector(7 downto 0);
            rx_fifo_valid_data : out std_logic;
            rx_fifo_read       : in std_logic
        );
    end component;

begin

    uart_rx_inst : uart_rx
    port map(
        clk                => clk,
        reset_n            => reset_n,
        rx                 => rx,
        rx_fifo            => rx_fifo,
        rx_fifo_valid_data => rx_fifo_valid_data,
        rx_fifo_read       => rx_fifo_read
    );
    
    clk     <= not clk after 10 ns;
    reset_n <= '0', '1' after 50 ns;
    
    gen_stimuli : process
    begin
        rx <= '1';
        
        -------------------------
        -- NORMALE ÜBERTRAGUNG --
        --------- 9600 Bd -------
        -------------------------
        -- 'A'
        wait for 100 ns;
        rx <= '0';
        rx_fifo_read <= '1', '0' after 20 ns;
        
        wait for BAUD_9600;
        rx <= '1';
        wait for BAUD_9600;
        rx <= '1';
        wait for BAUD_9600;
        rx <= '0';
        wait for BAUD_9600;
        rx <= '0';
        wait for BAUD_9600;
        rx <= '0';
        wait for BAUD_9600;
        rx <= '0';
        wait for BAUD_9600;
        rx <= '0';
        wait for BAUD_9600;
        rx <= '1';
        
        wait for BAUD_9600;
        rx <= '1';
        
        
        -- 'B'
        wait for BAUD_9600;
        rx <= '0';
        rx_fifo_read <= '1', '0' after 20 ns;
        
        wait for BAUD_9600;
        rx <= '1';
        wait for BAUD_9600;
        rx <= '1';
        wait for BAUD_9600;
        rx <= '0';
        wait for BAUD_9600;
        rx <= '0';
        wait for BAUD_9600;
        rx <= '0';
        wait for BAUD_9600;
        rx <= '0';
        wait for BAUD_9600;
        rx <= '1';
        wait for BAUD_9600;
        rx <= '0';
        
        wait for BAUD_9600;
        rx <= '1';
        
        -- 'C'
        wait for BAUD_9600;
        rx <= '0';
        rx_fifo_read <= '1', '0' after 20 ns;
        
        wait for BAUD_9600;
        rx <= '1';
        wait for BAUD_9600;
        rx <= '1';
        wait for BAUD_9600;
        rx <= '0';
        wait for BAUD_9600;
        rx <= '0';
        wait for BAUD_9600;
        rx <= '0';
        wait for BAUD_9600;
        rx <= '0';
        wait for BAUD_9600;
        rx <= '1';
        wait for BAUD_9600;
        rx <= '1';
        
        wait for BAUD_9600;
        rx <= '1';
        
        
        -- 'f'
        wait for BAUD_9600;
        rx <= '0';
        rx_fifo_read <= '1', '0' after 20 ns;
        
        wait for BAUD_9600;
        rx <= '0';
        wait for BAUD_9600;
        rx <= '1';
        wait for BAUD_9600;
        rx <= '1';
        wait for BAUD_9600;
        rx <= '0';
        wait for BAUD_9600;
        rx <= '0';
        wait for BAUD_9600;
        rx <= '1';
        wait for BAUD_9600;
        rx <= '1';
        wait for BAUD_9600;
        rx <= '0';
        
        wait for BAUD_9600;
        rx <= '1';
        
        -- 'f'
        wait for BAUD_9600;
        rx <= '0';
        rx_fifo_read <= '1', '0' after 20 ns;
        
        wait for BAUD_9600;
        rx <= '0';
        wait for BAUD_9600;
        rx <= '1';
        wait for BAUD_9600;
        rx <= '0';
        wait for BAUD_9600;
        rx <= '0';
        wait for BAUD_9600;
        rx <= '1';
        wait for BAUD_9600;
        rx <= '1';
        wait for BAUD_9600;
        rx <= '0';
        wait for BAUD_9600;
        rx <= '0';
        
        wait for BAUD_9600;
        rx <= '1';
        -----------------------------
        -- FEHLERHAFTE ÜBERTRAGUNG --
        -- STARTBIT VERFÄLSCHT ------
        -----------------------------
        wait for 1000 us;
        rx <= '0','1' after 100 ns;
        rx_fifo_read <= '1', '0' after 20 ns;
        wait for BAUD_9600;
        rx <= '1';
        wait for BAUD_9600;
        rx <= '0';
        wait for BAUD_9600;
        rx <= '1';
        wait for BAUD_9600;
        rx <= '1';
        wait for BAUD_9600;
        rx <= '0';
        wait for BAUD_9600;
        rx <= '0';
        wait for BAUD_9600;
        rx <= '1';
        wait for BAUD_9600;
        rx <= '1';
        
        -----------------------------
        -- FEHLERHAFTE ÜBERTRAGUNG --
        -- BAUDARTE =/= 9600 --------
        -- LEICHTE ABWEICHUNG -------
        -----------------------------
        wait for 1000 us;
        rx <= '0';
        rx_fifo_read <= '1', '0' after 20 ns;
        
        wait for BAUD_9600_f;
        rx <= '1';
        wait for BAUD_9600_f;
        rx <= '0';
        wait for BAUD_9600_f;
        rx <= '1';
        wait for BAUD_9600_f;
        rx <= '1';
        wait for BAUD_9600_f;
        rx <= '0';
        wait for BAUD_9600_f;
        rx <= '0';
        wait for BAUD_9600_f;
        rx <= '1';
        wait for BAUD_9600_f;
        rx <= '1';
        
        wait;
    end process;
end architecture;
