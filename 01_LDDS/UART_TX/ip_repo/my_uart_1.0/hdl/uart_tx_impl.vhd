library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity uart_tx is
    port (
        clk             : in std_logic;
        reset_n         : in std_logic;
        tx              : out std_logic;
        tx_fifo         : in std_logic_vector(7 downto 0);
        tx_fifo_empty   : out std_logic;
        tx_fifo_written : in std_logic
    );
end entity;

architecture Behavioral of uart_tx is
    constant BAUD:   integer := 9600; 
    constant WIDTH:  integer := 50000000/BAUD;
    
    signal tx_fifo_empty_int:   std_logic;
    signal tx_int:              std_logic;
    signal tx_fifo_int:         std_logic_vector(7 downto 0);
    signal counter_baud:        unsigned(16 downto 0);
    signal tx_en:               std_logic;
    signal new_char:            std_logic;
    signal counter_bits:        unsigned(3 downto 0);
    signal foo:                 std_logic;
    
    type TX_ZUSTAENDE is (IDLE, SENDEN);
    signal TX_ZUSTAND : TX_ZUSTAENDE;
    type STATES is (EMPTY, FULL);
    signal STATE : STATES;
begin

    ------------
    -- LÖSUNG --
    ------------


    ----------------------------
    -- Erzeugung der Baudrate --
    ----------------------------
    divider: process(clk) 
    begin
        if rising_edge(clk) then
            if reset_n='0' then
                tx_en <= '0';
                counter_baud <= (others=> '0');
            else
                tx_en <= '0';
                counter_baud <= counter_baud + 1;
                if counter_baud = WIDTH then
                    counter_baud <= (others=> '0');
                    tx_en <= '1';
                end if;
            end if;
        end if;
    end process divider;

    ----------------------------
    -- Sender serieller Daten --
    ----------------------------
    SEND: process(clk)
    begin
        if rising_edge(clk) then
            if reset_n = '0' then
                tx_int <= '1';
                tx_fifo_int <= (others => '0');
                TX_ZUSTAND <= IDLE;
                counter_bits <= (others => '0');
            else
                case TX_ZUSTAND is
                    when IDLE =>
                        if (tx_en = '1') and (new_char = '1') then
                            tx_int <= '0'; -- Startbit
                            tx_fifo_int <= tx_fifo;
                            TX_ZUSTAND <= SENDEN;
                        end if;
                    when SENDEN =>
                        if counter_bits > 7 and tx_en = '1' then
                            counter_bits <= (others => '0');
                            tx_int <= '1';
                            TX_ZUSTAND <= IDLE;
                        elsif tx_en = '1' then
                            tx_int <= tx_fifo_int(0);
                            tx_fifo_int <= '0' & tx_fifo_int(7 downto 1);
                            counter_bits <= counter_bits + 1;
                        end if;           
                end case;
            end if;
        end if;
    end process;
    
    ---------------------------------
    -- Setzes des Signals new_char --
    ---------------------------------
    flags: process(clk) 
    begin
        if rising_edge(clk) then
            if reset_n = '0' then
                tx_fifo_empty_int <= '1';
                new_char <= '0';
                STATE <= EMPTY;  
            else
                case STATE is
                    when EMPTY =>
                        if tx_fifo_written = '1' then
                            tx_fifo_empty_int <= '0';
                            new_char <= '1';
                            STATE <= FULL;            
                        end if;                
                    when FULL =>
                        if TX_ZUSTAND = IDLE and tx_en = '1' then
                            new_char <= '0';
                            tx_fifo_empty_int <= '1';   
                            STATE <= EMPTY;                     
                        end if;
                end case;
            end if;          
        end if;
    end process;

    tx_fifo_empty <= tx_fifo_empty_int;
    tx <= tx_int;



    ------------------
    -- MUSTERLÖSUNG --
    ------------------
    
    
--    handle_status_bits: process(clk)
--    begin
--        if rising_edge(clk) then
--            if reset_n = '0' then
--                tx_fifo_empty <= '1';
--                new_char <= '0';
--            else
--                if tx_fifo_written = '1' then
--                    new_char <= '1';
--                    tx_fifo_empty <= '0';
--                end if;
                
--                if (new_char = '1') and (TX_ZUSTAND = IDLE) and (tx_en = '1') then
--                    new_char <= '0';
--                    tx_fifo_empty <= '1';
--                end if;
--            end if;
--        end if;
--    end process;
    
--    serial_send:process(clk)
--    begin
--        if rising_edge(clk) then
--            if reset_n = '0' then
--                tx <= '1';
--                TX_ZUSTAND <= IDLE;
--                counter_bits <= (others => '0');
--                tx_fifo_int <= (others => '0');
--            elsif tx_en = '1' then
--                case TX_ZUSTAND is
--                    when IDLE =>
--                        if new_char = '1' then
--                            TX_ZUSTAND <= SENDEN;
--                            tx <= '0';
--                            tx_fifo_int <= tx_fifo;
--                        end if;
                        
--                    when SENDEN =>
--                        tx <= tx_fifo_int(0);
--                        tx_fifo_int <= '1' & tx_fifo_int(7 downto 1);
--                        counter_bits <= counter_bits + 1;
--                        if counter_bits = 8 then
--                            TX_ZUSTAND <= IDLE;
--                            counter_bits <= (others => '0');
--                        end if;
--                    when others =>
--                        TX_ZUSTAND <= IDLE;
--                end case;
--            end if;
--        end if;
--    end process;

end architecture;
