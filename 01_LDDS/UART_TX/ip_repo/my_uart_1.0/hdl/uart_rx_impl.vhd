library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity uart_rx is
    port (
        clk                : in std_logic;
        reset_n            : in std_logic;
        rx                 : in std_logic;
        rx_fifo            : out std_logic_vector(7 downto 0);
        rx_fifo_valid_data : out std_logic;
        rx_fifo_read       : in std_logic
    );
end entity;

architecture Behavioral of uart_rx is
	constant BAUD: 	       integer := 9600;
	constant FREQ:         integer := 50000000;
	constant WIDTH:        integer := FREQ/BAUD;
	constant HALF_WIDTH:   integer := WIDTH/2;
	
	signal counter_baud:        unsigned(16 downto 0);
    signal counter_clk_div_en:  unsigned(11 downto 0);
	signal rx_prev:	            std_logic;
	signal rx_en:		        std_logic;
	signal rx_bit:		        unsigned(4 downto 0);
	signal rx_fifo_int:         std_logic_vector(8 downto 0);
	signal rx_comm_fin:         std_logic;
	signal rx_data_read:        std_logic; -- Indiziert, ob der AXI-Bus die Daten aus rx_fifo gelesen hat und weitere Daten empfangen werden d�rfen.
	
	type RX_STATES is (FALL_EDG, GEN_EN, LAST_BIT);
	signal RX_STATE : RX_STATES;

begin
    ---------------------------
    -- GENERIERUNG VON RX_EN --
    ---------------------------
	clk_en_generator: process(clk)
	begin
		if rising_edge(clk) then
			if reset_n = '0' then
				counter_baud <= (others => '0');
				counter_clk_div_en <= (others => '0');
				RX_STATE <= LAST_BIT;
				rx_bit <= (others => '0');
				rx_en <= '0';
				rx_prev <= '0';
                rx_comm_fin <= '0';
			else
                case RX_STATE is
                    when LAST_BIT =>
                        rx_comm_fin <= '0';
                        if rx_prev = '1' and rx = '0' then
                            RX_STATE <= FALL_EDG;
                        else
                            rx_prev <= rx;
                        end if;
                    when FALL_EDG =>
                        --------------------------------------
                        -- VERZ�GERUNG UM EINEN HALBEN TAKT --
                        --------------------------------------
                        counter_clk_div_en <= counter_clk_div_en + 1;
                        if counter_clk_div_en = HALF_WIDTH then
                            counter_clk_div_en <= (others => '0');
                            if rx_data_read = '1' then
                                rx_en <= '1';
                                RX_STATE <= GEN_EN;
                            end if;
                        end if;
                    when GEN_EN =>
                        ---------------------------------------
                        -- CLOCK GENERATOR F�R DIE ABTASTUNG --
                        ---------------------------------------
                        counter_baud <= counter_baud + 1;
                        rx_en <= '0';
                        if counter_baud >= WIDTH then
                            counter_baud <= (others => '0');
                            rx_en <= '1';
                            rx_bit <= rx_bit + 1;
                            
                            if rx_bit >= 8 then
                                rx_comm_fin <= '1';
                                rx_en <= '0';
                                rx_prev <= '0';
                                RX_STATE <= LAST_BIT;
                                rx_bit <= (others => '0');
                            end if;
                        end if;
                end case;
            end if;
        end if;
	end process;
	
	
	-------------------------------------------------
	-- Eigentliche RX-Funktion mit Schieberegister --
	-------------------------------------------------	
	READ:process(clk)
	begin
	   if rising_edge(clk) then
	       if reset_n = '0' then
	           rx_fifo_int <= (others => '0');
	       else
	           if rx_en = '1' then
	               rx_fifo_int <= rx & rx_fifo_int(8 downto 1);
               elsif rx_fifo_read = '1' then
                   rx_fifo_int <= (others => '0');
	           end if;
	       end if;
	   end if;
	end process;
	
	---------------------------------------------------
	-- FEHLER ABFANGEN UND RX_FIFO_VALID_DATA SETZEN --
	---------------------------------------------------
	VALIDATION: process(clk)
	begin
	   if rising_edge(clk) then
	       if reset_n = '0' then
	           rx_fifo_valid_data <= '0';
	           rx_data_read <= '0';
	           rx_fifo <= rx_fifo_int(8 downto 1);
	       else
               if rx_comm_fin = '1' then
                    if rx_fifo_int(0) = '0' AND rx = '1' then -- Startbit = 0 and Stoppbit = 1
                        rx_fifo_valid_data <= '1';
                        rx_data_read <= '0';
                        rx_fifo <= rx_fifo_int(8 downto 1);
                    else
                        rx_fifo_valid_data <= '0';    
                    end if;
               end if;
               
               if rx_fifo_read = '1' then
                   rx_fifo_valid_data <= '0';
                   rx_data_read <= '1';
               end if;
	       end if;
	   end if;
	end process;
end architecture;


architecture Verhalten of uart_rx is
    constant BAUD: 	       integer := 9600;
	constant FREQ:         integer := 50000000;
	constant WIDTH:        integer := FREQ/BAUD;
	constant HALF_WIDTH:   integer := WIDTH/2;
	
	signal counter_baud:        unsigned(16 downto 0);
    signal counter_delay:       unsigned(11 downto 0);
	signal rx_prev:	            std_logic;
	signal rx_en:		        std_logic;
	signal rx_bit:		        unsigned(4 downto 0);
	signal rx_fifo_int:         std_logic_vector(8 downto 0);
	signal rx_data_read:        std_logic; -- Indiziert, ob der AXI-Bus die Daten aus rx_fifo gelesen hat und weitere Daten empfangen werden d�rfen.
	
	type RX_STATES is (Z1, Z2, Z3, Z4);
	signal RX_STATE : RX_STATES;
begin
    rx_automat: process(clk)
    begin
        if rising_edge(clk) then
            if reset_n = '0' then
                counter_baud <= (others => '0');
                counter_delay <= (others => '0');
                rx_en <= '0';
                RX_STATE <= Z1;
                rx_bit <= (others => '0');
                rx_fifo_int <= (others => '0');
                rx_fifo <= (others => '0');
                rx_fifo_valid_data <= '0';
            end if;
            case RX_STATE is
                when Z1 => -- IDLE
                    if rx = '0' then
                        RX_STATE <= Z2;
                    end if;
                when Z2 => -- VERZ�GERUNG
                    counter_delay <= counter_delay + 1;
                    if counter_delay >= HALF_WIDTH then
                        RX_STATE <= Z3;
                        counter_delay <= (others => '0');
                    end if;
                when Z3 => -- ABTASTUNG
                    if rx_bit <= 8 then
                        counter_baud <= counter_baud + 1;
                        rx_en <= '0';
                        if counter_baud = WIDTH then
                            counter_baud <= (others => '0');
                            rx_en <= '1';
                            rx_fifo_int <= rx & rx_fifo_int(8 downto 1);
                            rx_bit <= rx_bit + 1;
                        end if;
                    else
                        rx_en <= '0';
                        rx_bit <= (others => '0');
                        RX_STATE <= Z4;
                    end if;
                when Z4 => -- VALIDIERUNG UND FREIGABE
                    if rx_fifo_int(8) = '1' then
                        rx_fifo_valid_data <= '1';
                        rx_fifo <= rx_fifo_int(7 downto 0);
                        
                        if rx_fifo_read = '1' then
                            rx_fifo_int <= (others => '0');
                            RX_STATE <= Z1;
                            rx_fifo_valid_data <= '0';
                        end if;
                    end if;
            end case;
        end if;
    end process;
end architecture;