library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity testbench_rx is
end entity;

architecture Behavioral of testbench_rx is

    signal clk                : std_logic                    := '0';
    signal reset_n            : std_logic                    := '0';
    signal rx                 : std_logic                    := '1';
    signal rx_fifo            : std_logic_vector(7 downto 0) := (others => '0');
    signal rx_fifo_valid_data : std_logic                    := '0';
    signal rx_fifo_read       : std_logic                    := '0';

    component uart_rx is
        port (
            clk                : in std_logic;
            reset_n            : in std_logic;
            rx                 : in std_logic;
            rx_fifo            : out std_logic_vector(7 downto 0);
            rx_fifo_valid_data : out std_logic;
            rx_fifo_read       : in std_logic
        );
    end component;

begin

    uart_rx_inst : uart_rx
    port map(
        clk                => clk,
        reset_n            => reset_n,
        rx                 => rx,
        rx_fifo            => rx_fifo,
        rx_fifo_valid_data => rx_fifo_valid_data,
        rx_fifo_read       => rx_fifo_read
    );

end architecture;
