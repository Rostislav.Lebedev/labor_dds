library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity testbench_tx is
end entity;

architecture Behavioral of testbench_tx is
constant clk_period: time := 10 ns;
    signal clk             : std_logic                    := '0';
    signal reset_n         : std_logic                    := '0';
    signal tx              : std_logic                    := '0';
    signal tx_fifo         : std_logic_vector(7 downto 0) := (others => '0');
    signal tx_fifo_empty   : std_logic                    := '0';
    signal tx_fifo_written : std_logic                    := '0';

    component uart_tx is
        port (
            clk             : in std_logic;
            reset_n         : in std_logic;
            tx              : out std_logic;
            tx_fifo         : in std_logic_vector(7 downto 0);
            tx_fifo_empty   : out std_logic;
            tx_fifo_written : in std_logic
        );
    end component;

begin

    uart_tx_inst : uart_tx
    port map(
        clk             => clk,
        reset_n         => reset_n,
        tx              => tx,
        tx_fifo         => tx_fifo,
        tx_fifo_empty   => tx_fifo_empty,
        tx_fifo_written => tx_fifo_written
    );
    
    clk <= not clk after clk_period;
    reset_n <= '0', '1' after 10*clk_period;
    
    gen_stimuli : process
    begin
        tx_fifo <= "00000000";
        wait until reset_n = '1';
        tx_fifo <= "10001101";
        tx_fifo_written <= '1' after 11*clk_period, '0' after 12*clk_period;
        wait for 500 us;
        tx_fifo <= "11100001";
        tx_fifo_written <= '1' after 11*clk_period, '0' after 12*clk_period;
        wait;
    end process;

end architecture;
