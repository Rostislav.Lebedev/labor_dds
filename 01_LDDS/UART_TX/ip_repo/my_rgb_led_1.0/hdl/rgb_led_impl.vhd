-- LABOR DDS
-- TERMIN 1
-- PWM RGB LED SIMULATION
-- JURAS, LEBEDEV

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity rgb_led is
    port (
        clk     : in std_logic;
        reset_n : in std_logic;
        color   : in std_logic_vector(23 downto 0);
        led     : out std_logic_vector(2 downto 0)
    );
end entity;

architecture Behavioral of rgb_led is
    signal sig_count:   unsigned(7 downto 0);
    signal sig_ce:      std_logic;
    signal sig_led:     std_logic_vector(2 downto 0);
    signal col_r:       unsigned(7 downto 0);
    signal col_g:       unsigned(7 downto 0);
    signal col_b:       unsigned(7 downto 0);
begin

    col_r <= unsigned(color(23 downto 16));
    col_g <= unsigned(color(15 downto 8));
    col_b <= unsigned(color(7 downto 0));
    
    clock_divider:process(clk)
    begin
        if rising_edge(clk) then
            if reset_n = '0' then
                sig_count <= (others => '0');
                sig_ce <= '0';
            else
                sig_ce <= '0';
                sig_count <= sig_count + 1;
                if sig_count = 195 then             -- 50MHz/256kHz = 195,3125
                    sig_count <= (others => '0');
                    sig_ce <= '1';
                end if;
            end if;
        end if;
    end process;
    
    
    -- clk statt sig_ce!!!
    -- SYNCHRONES DESIGN!!!        
    PWM_generator:process(sig_ce)
        variable var_count: unsigned (7 downto 0) := (others => '0');
    begin
        if rising_edge(sig_ce) then
            if reset_n = '0' then
                var_count := (others => '0');
                led <= (others => '0');
                sig_led <= (others => '0');
            else
                var_count := var_count + 1;
                if var_count = 255 then
                    var_count := (others => '0');
                end if;
                
                if var_count < col_r then
                    sig_led(2) <= '1'; 
                else
                    sig_led(2) <= '0';
                end if;
                
                if var_count < col_g then
                    sig_led(1) <= '1';
                else
                    sig_led(1) <= '0';
                end if;
                
                if var_count < col_b then
                    sig_led(0) <= '1';
                else
                    sig_led(0) <= '0';
                end if;
            end if;
        end if;
        led <= sig_led;
    end process;
end architecture;
