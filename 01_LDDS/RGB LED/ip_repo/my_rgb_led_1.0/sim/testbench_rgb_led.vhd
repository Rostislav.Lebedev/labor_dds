-- LABOR DDS
-- TERMIN 1
-- PWM RGB LED SIMULATION
-- JURAS, LEBEDEV

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity testbench is
end testbench;

architecture Behavioral of testbench is

    constant clk_period: time := 20 ns; -- 1/50 MHz = 20 ns

    signal clk     : std_logic                     := '0';
    signal reset_n : std_logic                     := '0';
    signal color   : std_logic_vector(23 downto 0) := (others => '0');
    signal led     : std_logic_vector(2 downto 0)  := (others => '0');

    component rgb_led is
        port (
            clk     : in std_logic;
            reset_n : in std_logic;
            color   : in std_logic_vector(23 downto 0);
            led     : out std_logic_vector(2 downto 0)
        );
    end component;

begin

    rgb_led_inst : rgb_led
    port map(
        clk     => clk,
        reset_n => reset_n,
        color   => color,
        led     => led
    );
    
    clk <= not clk after (clk_period);
    reset_n <= '0', '1' after (clk_period * 10);
    
    generate_stimuli: process
    begin
        color <= x"000000";
        wait until reset_n = '1';
        
        color <= x"1080f0";         -- Beispiel (hellblau)
        wait for 6000 us;
        
        color <= x"7f01fe";         -- Beispiel (hellblau)
        wait for 6000 us;
        -- REGENBOGEN --
--        
--        color <= x"ff0000";         -- rot
        
--        wait for 6000 us;
--        color <= x"ff8800";         -- orange
        
--        wait for 6000 us;
--        color <= x"ffff00";         -- gelb
        
--        wait for 6000 us;
--        color <= x"00ff00";         -- gr�n
        
--        wait for 6000 us;
--        color <= x"0000ff";         -- blau
        
--        wait for 6000 us;
--        color <= x"0080ff";         -- indigo
        
--        wait for 6000 us;
--        color <= x"7f00ff";         -- violett
        
        wait;
    end process;
end Behavioral;
