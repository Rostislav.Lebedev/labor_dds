library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity uart_rx is
    port (
        clk                : in std_logic;
        reset_n            : in std_logic;
        rx                 : in std_logic;
        rx_fifo            : out std_logic_vector(7 downto 0);
        rx_fifo_valid_data : out std_logic;
        rx_fifo_read       : in std_logic
    );
end entity;

architecture Behavioral of uart_rx is

begin

    rx_fifo            <= (others => '0');
    rx_fifo_valid_data <= '0';

end architecture;
