library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity uart_tx is
    port (
        clk             : in std_logic;
        reset_n         : in std_logic;
        tx              : out std_logic;
        tx_fifo         : in std_logic_vector(7 downto 0);
        tx_fifo_empty   : out std_logic;
        tx_fifo_written : in std_logic
    );
end entity;

architecture Behavioral of uart_tx is
    signal baud_count:  unsigned (12 downto 0);
    signal baud_en:     std_logic;
begin

    tx            <= '1';
    tx_fifo_empty <= '0';
    
    baud_divider:process(clk)
    begin
        if rising_edge(clk) then
            if reset_n = '0' then
                baud_count <= (others => '0');
                baud_en <= '0';
            else
                baud_en <= '0';
                baud_count <= baud_count + 1;
                if baud_count = 5208 then
                    baud_count <= (others => '0');
                    baud_en <= '1';
                end if;
            end if;
        end if;
    end process; 
    
   

end architecture;
