# Usage with Vitis IDE:
# In Vitis IDE create a Single Application Debug launch configuration,
# change the debug type to 'Attach to running target' and provide this 
# tcl script in 'Execute Script' option.
# Path of this script: C:\Users\Rostislav\xilinx\ldds_rgb_led_vitis2_system\_ide\scripts\debugger_ldds_rgb_led_vitis2-default.tcl
# 
# 
# Usage with xsct:
# To debug using xsct, launch xsct and run below command
# source C:\Users\Rostislav\xilinx\ldds_rgb_led_vitis2_system\_ide\scripts\debugger_ldds_rgb_led_vitis2-default.tcl
# 
connect -url tcp:127.0.0.1:3121
targets -set -nocase -filter {name =~"APU*"}
rst -system
after 3000
targets -set -filter {jtag_cable_name =~ "Digilent Cora Z7 - 7007S 210370AFD136A" && level==0 && jtag_device_ctx=="jsn-Cora Z7 - 7007S-210370AFD136A-13723093-0"}
fpga -file C:/Users/Rostislav/xilinx/ldds_rgb_led_vitis2/_ide/bitstream/block_design_wrapper.bit
targets -set -nocase -filter {name =~"APU*"}
loadhw -hw C:/Users/Rostislav/xilinx/ldds_platform2/export/ldds_platform2/hw/block_design_wrapper.xsa -mem-ranges [list {0x40000000 0xbfffffff}] -regs
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*"}
source C:/Users/Rostislav/xilinx/ldds_rgb_led_vitis2/_ide/psinit/ps7_init.tcl
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "*A9*#0"}
dow C:/Users/Rostislav/xilinx/ldds_rgb_led_vitis2/Release/ldds_rgb_led_vitis2.elf
configparams force-mem-access 0
targets -set -nocase -filter {name =~ "*A9*#0"}
con
