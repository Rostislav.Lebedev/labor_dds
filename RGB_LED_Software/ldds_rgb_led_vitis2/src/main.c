#include <xparameters.h>
#include <stdint.h>
#include <sleep.h>

int main() {
    volatile uint32_t* led0_color = (uint32_t*)(XPAR_MY_RGB_LED_0_S00_AXI_BASEADDR);

    *led0_color = 0x0000ff; // LED rot leuchten lassen

    while (1) {
        // Endlosschleife
    	/*
    	*led0_color = 0xff0000;
    	sleep(1);
    	*led0_color = 0x00ff00;
    	sleep(1);
    	*led0_color = 0x0000ff;
    	sleep(1);
    	*led0_color = 0xffffff;
    	sleep(1);
    	*/
    }
}
